Business Name: El Bufete De Defensa Criminal

Address: 3500 Fifth Ave, Office 121, San Diego, CA 92103 USA

Phone: (619) 722-5858

Website: https://www.thecriminaldefensa.com

Description: If you need a San Diego criminal defense attorney and live in or around San Diego, California look no further. Criminal defense attorney Kerry Steigerwalt is a San Diego domestic violence, DUI, and criminal defense attorney for assault and battery. Anyone accused of misdemeanor or serious crime through the criminal justice system wants the best defense attorney to help them. Gaining knowledge and experience throughout the process is essential. You need to take the time to search thoroughly until, according to your judgment, you find the top criminal defense attorney in your area.

Keywords: Aggravated assault, Assault and battery, Child abuse, Child danger, DUI defense, Fraud Defense, Grand Theft, Defense of homicides, Nursing license.
